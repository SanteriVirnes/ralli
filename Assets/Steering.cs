﻿using UnityEngine;
using System.Collections;

public class Steering : MonoBehaviour {

	// Forward motion.
	public float speed = 0f;
	public float topSpeed = 2f;
	public float forwardAcceleration = 0.001f;

	// Steering.
	public float turnSpeed = 0f;
	public float maximumTurnvalue = 2f;
	public float angularAcceleration = 0.03f;


	/// <summary>
	/// Initialization steps.
	/// </summary>
	void Start(){

	}

	/// <summary>
	///  Update steps.
	/// </summary>
	void Update(){

		// Get user input for forward movement.
		if (Input.GetKey(KeyCode.UpArrow))
		{
			if (speed + forwardAcceleration <= topSpeed)
				speed += forwardAcceleration;
			else
				speed = topSpeed;
		}

		// Get user input for backward movement.
		else if (Input.GetKey(KeyCode.DownArrow))
		{
			if (speed - forwardAcceleration >= -topSpeed)
				speed -= forwardAcceleration;
			else
				speed = -topSpeed;
		}

		// Get user input for steering to left.
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			if (turnSpeed - angularAcceleration >= -maximumTurnvalue)
				turnSpeed -= angularAcceleration;
			else
				turnSpeed = -maximumTurnvalue;
		}

		// Get user input for steering to right.
		if (Input.GetKey(KeyCode.RightArrow))
		{
			if (turnSpeed + angularAcceleration <= maximumTurnvalue)
				turnSpeed += angularAcceleration;
			else
				turnSpeed = maximumTurnvalue;
		}

		// Update movement and steering.
		Move (speed);		
		Rotate (turnSpeed);


	}

	/// <summary>
	/// Move the vehicle by appying changes to transform.
	/// </summary>
	/// <param name="speed">Speed of the vehicle.</param>
	void Move(float speed){
		transform.Translate(0, 0, speed);
	}

	/// <summary>
	/// Steer the vehicle by appying changes to transform.
	/// </summary>
	/// <param name="steering">Steering for the vehicle.</param>
	void Rotate(float steering){
		transform.Rotate(0, steering, 0);
	}
	
	
}

