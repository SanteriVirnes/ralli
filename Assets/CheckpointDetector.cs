﻿using UnityEngine;
using System.Collections;

public class CheckpointDetector : MonoBehaviour {

	/// <summary>
	/// Initialization steps.
	/// </summary>
	void Start () {
	
	}
	
	/// <summary>
	/// Update steps.
	/// </summary>
	void Update () {

		// Find colliders overlapping with check point and check whether one of them is player.
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, 12.0f);
		int i = 0;
		while (i < hitColliders.Length) {
			if (hitColliders[i].gameObject.tag == "Player")
			{
				Debug.Log("You reached the checpoint");
				break;
			}
			i++;
		}

	
	}
}
